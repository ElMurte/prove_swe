import 'package:background_location/background_location.dart';
import 'package:flutter/material.dart';
import 'package:prove_swe/classes/CustomElevatedButton.dart';
import 'package:prove_swe/pages/HomeGPS.dart';
import 'package:prove_swe/pages/HomeNFC.dart';

class Home extends StatefulWidget {
  final double fontSizeButton = 30;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('SWE Tests')),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: CustomElevatedButton.homeButtonStyle(),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomeNFC()));
                },
                child: Text('NFC Test',
                    style: TextStyle(fontSize: widget.fontSizeButton),
                    textAlign: TextAlign.center),
              ),
              ElevatedButton(
                style: CustomElevatedButton.homeButtonStyle(),
                onPressed: () async {
                  var wantsGrant = await _showGPSDialogAsk();
                  if (!wantsGrant) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                      '🤬',
                      textAlign: TextAlign.center,
                    )));
                    return;
                  }
                  BackgroundLocation.getPermissions(onGranted: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeGPS()));
                  }, onDenied: () {
                    _showGPSDialogFail();
                  });
                },
                child: Text('GPS Test',
                    style: TextStyle(fontSize: widget.fontSizeButton),
                    textAlign: TextAlign.center),
              )
            ],
          ),
        ],
      ),
    );
  }

  Future<bool> _showGPSDialogAsk() async {
    var status = await BackgroundLocation.checkPermissions();
    if (status.toString() == 'PermissionStatus.granted') {
      return true;
    }
    var ris = false;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('GPS permission'),
            content: Text(
                'Please allow the usage of GPS to use this section of the app.'),
            actions: <Widget>[
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).pop();
                    ris = true;
                  },
                  icon: Icon(Icons.explore),
                  label: Text('OK')),
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).pop();
                    ris = false;
                  },
                  icon: Icon(Icons.explore_off_outlined),
                  label: Text('NO'))
            ],
          );
        });
    return ris;
  }

  void _showGPSDialogFail() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('GPS Permission denied'),
            content: Text(
                'This section of app needs the GPS permission. Go to settings and grant it.'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Understood'),
              )
            ],
          );
        });
  }
}
