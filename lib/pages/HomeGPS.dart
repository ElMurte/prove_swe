import 'package:background_location/background_location.dart';
import 'package:flutter/material.dart';
import 'package:prove_swe/classes/CustomElevatedButton.dart';

class HomeGPS extends StatefulWidget {
  final double _fontSize = 20;
  @override
  _HomeGPSState createState() => _HomeGPSState();
}

class _HomeGPSState extends State<HomeGPS> {
  bool _started = false;
  String _latitude = 'waiting...';
  String _longitude = 'waiting...';
  String _accuracy = 'waiting...';
  String _speed = 'waiting...';
  String _time = 'waiting...';
  final List<Widget> _history = [
    Text('HISTORY', style: TextStyle(fontSize: 30))
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GPS Test'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    TextData('Latitude: ' + _latitude),
                    TextData('Longitude: ' + _longitude),
                    TextData('Accuracy: ' + _accuracy),
                    TextData('Speed: ' + _speed),
                    TextData('Time: ' + _time),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(widget._fontSize / 2),
            child: ElevatedButton(
              onPressed: () async {
                if (_started) return;
                await BackgroundLocation.setAndroidNotification(
                  title: 'Background service is running',
                  message: 'Background location in progress',
                  icon: '@mipmap/ic_launcher',
                );
                await BackgroundLocation.setAndroidConfiguration(1000);
                await BackgroundLocation.startLocationService();
                _started = true;
                BackgroundLocation.getLocationUpdates((location) {
                  setState(() {
                    _latitude = location.latitude.toString();
                    _longitude = location.longitude.toString();
                    _accuracy = location.accuracy.toString();
                    _speed = location.speed.toStringAsFixed(2);
                    _time = DateTime.fromMillisecondsSinceEpoch(
                            location.time.toInt())
                        .toString();
                    _history.add(Text(_time));
                  });
                });
              },
              child: Text('Start Location Service',
                  style: TextStyle(fontSize: widget._fontSize + 5)),
              style: CustomElevatedButton.homeButtonStyle(),
            ),
          ),
          Container(
            padding: EdgeInsets.all(widget._fontSize / 2),
            child: ElevatedButton(
              onPressed: () {
                BackgroundLocation.stopLocationService();
                _started = false;
              },
              child: Text('Stop Location Service',
                  style: TextStyle(fontSize: widget._fontSize + 5)),
              style: CustomElevatedButton.homeButtonStyle(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: _history,
              )
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    BackgroundLocation.stopLocationService();
    super.dispose();
  }

  Widget TextData(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: widget._fontSize, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    );
  }
}
