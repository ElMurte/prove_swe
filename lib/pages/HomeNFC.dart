import 'package:flutter/material.dart';
import 'package:flutter_nfc_reader/flutter_nfc_reader.dart';
import 'package:prove_swe/classes/CustomBoxDecoration.dart';

class HomeNFC extends StatefulWidget {
  @override
  _HomeNFCState createState() => _HomeNFCState();
}

class _HomeNFCState extends State<HomeNFC> {
  String _scan = 'No scan result';
  bool scanEnabled = true;
  bool stopEnabled = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('NFC Test'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(
                iconSize: 100,
                icon: Icon(Icons.nfc),
                tooltip: 'Scan NFC tag',
                onPressed: scanEnabled
                    ? () {
                        setState(() {
                          _scan = 'Reading...';
                          scanEnabled = false;
                          stopEnabled = true;
                        });
                        _read().then((data) => {
                              setState(() {
                                _scan = data.content;
                              })
                            });
                      }
                    : null,
              ),
              IconButton(
                  iconSize: 100,
                  icon: Icon(Icons.stop_circle_outlined),
                  tooltip: 'Stop NFC scan',
                  onPressed: stopEnabled
                      ? () {
                          _stop();
                          setState(() {
                            scanEnabled = true;
                            stopEnabled = false;
                            _scan = 'Stopped';
                          });
                        }
                      : null)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  decoration: CustomBoxDecoration().resultBoxDecoration(),
                  padding: EdgeInsets.all(5),
                  child: Text(
                    _scan,
                    style: TextStyle(fontSize: 25),
                    textAlign: TextAlign.center,
                  ))
            ],
          ),
        ],
      ),
    );
  }

  Future<NfcData> _read() async {
    return await FlutterNfcReader.read();
  }

  void _stop() {
    FlutterNfcReader.stop();
  }
}
