import 'package:flutter/material.dart';
import 'package:prove_swe/pages/Home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Color accentColor = Colors.orange[300];
  final Color primaryColor = Colors.grey[700];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.from(colorScheme: ColorScheme.light()).copyWith(
          primaryColor: primaryColor,
          accentColor: accentColor,
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(primary: primaryColor))),
      title: 'SWE',
      home: Home(),
    );
  }
}
