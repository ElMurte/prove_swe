import 'package:flutter/material.dart';

class CustomBoxDecoration {
  BoxDecoration resultBoxDecoration() {
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: Colors.grey[300]);
  }
}
