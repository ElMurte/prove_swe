import 'package:flutter/material.dart';

class CustomElevatedButton {
  static ButtonStyle homeButtonStyle() {
    return ElevatedButton.styleFrom(
        padding: EdgeInsets.all(10),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)));
  }
}
